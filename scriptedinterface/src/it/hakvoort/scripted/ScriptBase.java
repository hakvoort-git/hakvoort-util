package it.hakvoort.scripted;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The ScriptBase annotates the script file which should be evaluated in order to
 * retrieve the backing script object for the scripted interface.
 * 
 * @author Michiel Hakvoort <michiel@hakvoort.it>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ScriptBase {

	public String value();
}
