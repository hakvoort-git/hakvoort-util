package it.hakvoort.scripted;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The ScriptRequired annotation can be used to specify additional script
 * files which need to be loaded upon scriptable creation.
 * 
 * @author Michiel Hakvoort <michiel@hakvoort.it>
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ScriptRequired {

	public String[] value();
}
