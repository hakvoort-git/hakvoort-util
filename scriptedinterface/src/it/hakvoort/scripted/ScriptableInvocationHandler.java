package it.hakvoort.scripted;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import javax.script.Invocable;

/**
 * The invocation handler for {@link ScriptBase scripted} interfaces.
 * 
 * @author Michiel Hakvoort <michiel@hakvoort.it>
 *
 */
public class ScriptableInvocationHandler implements InvocationHandler {

	protected Invocable invocable;
	protected Object target;

	/**
	 * Construct a new {@link ScriptableInvocationHandler}
	 * 
	 * @param invocable The {@link Invocable invocable} mediator which handles invocations on the given target
	 * @param target The object backing
	 */
	public ScriptableInvocationHandler(Invocable invocable, Object target) {
		this.invocable = invocable;
		this.target = target;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		return invocable.invokeMethod(target, method.getName(), args);
	}

}